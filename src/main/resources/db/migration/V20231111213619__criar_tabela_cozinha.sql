create table cozinha
(
    id                  integer      not null auto_increment primary key key,
    codigo              varchar(36)  not null,
    nome                varchar(255) not null,
    data_hora_inclusao    datetime     not null DEFAULT (UTC_TIMESTAMP),
    data_hora_atualizacao datetime
) engine = InnoDB
  default charset = utf8mb4