create table restaurante
(
    id                    bigint auto_increment primary key,
    codigo                varchar(36)  not null,
    nome                  varchar(255) not null,
    taxa_frete            decimal      not null,
    cozinha_id            bigint       not null,
    data_hora_inclusao    datetime     not null DEFAULT (UTC_TIMESTAMP),
    data_hora_atualizacao datetime
) engine = InnoDB
  default charset = utf8mb4