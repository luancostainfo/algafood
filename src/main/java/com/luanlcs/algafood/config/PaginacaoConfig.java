package com.luanlcs.algafood.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.data.domain.Page;

import java.io.IOException;

@JsonComponent
public class PaginacaoConfig extends JsonSerializer<Page<?>> {

    @Override
    public void serialize(Page<?> page, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        jsonGenerator.writeStartObject();

        jsonGenerator.writeObjectField("conteudo", page.getContent());
        jsonGenerator.writeObjectField("paginaAtual", page.getNumber());
        jsonGenerator.writeObjectField("itensPorPagina", page.getSize());
        jsonGenerator.writeObjectField("totalDeItens", page.getTotalElements());
        jsonGenerator.writeObjectField("totalDePaginas", page.getTotalPages());

        jsonGenerator.writeEndObject();
    }
}
