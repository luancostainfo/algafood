package com.luanlcs.algafood.api.dto.output;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ListagemResturanteOutput {

    private Long id;
    private String nome;
    private BigDecimal taxaFrete;
    private String cozinha;

}
