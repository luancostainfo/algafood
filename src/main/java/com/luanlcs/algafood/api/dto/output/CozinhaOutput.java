package com.luanlcs.algafood.api.dto.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CozinhaOutput {

    private Long id;
    private String codigo;
    private String nome;

}
