package com.luanlcs.algafood.api.dto.input;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CozinhaInput {

    @Size(min = 3, max = 255)
    @NotBlank
    private String nome;

}
