package com.luanlcs.algafood.api.dto.filter;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class RestauranteFilter {

    private String nome;
    private BigDecimal taxaFreteInicial;
    private BigDecimal taxaFreteFinal;
    private Long cozinhaId;

}
