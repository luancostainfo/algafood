package com.luanlcs.algafood.api.dto.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CozinhaFilter {

    private String nome;

}
