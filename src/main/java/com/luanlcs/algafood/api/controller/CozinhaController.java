package com.luanlcs.algafood.api.controller;

import com.luanlcs.algafood.api.dto.filter.CozinhaFilter;
import com.luanlcs.algafood.api.dto.input.CozinhaInput;
import com.luanlcs.algafood.api.dto.output.CozinhaOutput;
import com.luanlcs.algafood.domain.service.cozinha.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RequiredArgsConstructor
@RestController
@RequestMapping("/cozinhas")
public class CozinhaController {

    private final ListarCozinhaService listarCozinhaService;
    private final BuscarCozinhaService buscarCozinhaService;
    private final CadastrarCozinhaService cadastrarCozinhaService;
    private final AtualizarCozinhaService atualizarCozinhaService;
    private final ExcluirCozinhaService excluirCozinhaService;

    @GetMapping
    public Page<CozinhaOutput> listarTodas(CozinhaFilter cozinhaFilter, Pageable pageable) {
        return listarCozinhaService.executar(cozinhaFilter, pageable);
    }

    @GetMapping("/{id}")
    public CozinhaOutput buscarPorId(@PathVariable Long id) {
        return buscarCozinhaService.executar(id);
    }

    @PostMapping
    public ResponseEntity<CozinhaOutput> cadastrar(@Valid @RequestBody CozinhaInput cozinhaInput) {
        CozinhaOutput cozinhaOutput = cadastrarCozinhaService.executar(cozinhaInput);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(cozinhaOutput.getId())
                .toUri();

        return ResponseEntity.created(uri).body(cozinhaOutput);
    }


    @PutMapping("/{id}")
    public CozinhaOutput atualizar(@PathVariable Long id, @Valid @RequestBody CozinhaInput cozinhaInput) {
        return atualizarCozinhaService.executar(id, cozinhaInput);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable Long id) {
        excluirCozinhaService.executar(id);
    }
}
