package com.luanlcs.algafood.api.controller;

import com.luanlcs.algafood.api.dto.filter.RestauranteFilter;
import com.luanlcs.algafood.api.dto.output.ListagemResturanteOutput;
import com.luanlcs.algafood.domain.service.restaurante.ListarRestauranteService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/restaurantes")
public class RestauranteController {

    private final ListarRestauranteService listarRestauranteService;

    @GetMapping
    private Page<ListagemResturanteOutput> listar(RestauranteFilter restauranteFilter, Pageable pageable) {
        return listarRestauranteService.executar(restauranteFilter, pageable);
    }

}
