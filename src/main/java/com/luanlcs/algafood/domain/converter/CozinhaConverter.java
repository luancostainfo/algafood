package com.luanlcs.algafood.domain.converter;

import com.luanlcs.algafood.api.dto.input.CozinhaInput;
import com.luanlcs.algafood.api.dto.output.CozinhaOutput;
import com.luanlcs.algafood.domain.model.Cozinha;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CozinhaConverter {

    public CozinhaOutput toOutput(Cozinha cozinha) {
        CozinhaOutput cozinhaOutput = new CozinhaOutput();
        cozinhaOutput.setId(cozinha.getId());
        cozinhaOutput.setCodigo(cozinha.getCodigo());
        cozinhaOutput.setNome(cozinha.getNome());
        return cozinhaOutput;
    }

    public List<CozinhaOutput> toOutputList(Collection<Cozinha> cozinhas) {
        return cozinhas.stream().map(this::toOutput).collect(Collectors.toList());
    }

    public Cozinha toEntity(CozinhaInput cozinhaInput) {
        Cozinha cozinha = new Cozinha();
        cozinha.setNome(cozinhaInput.getNome());
        return cozinha;
    }

    public void copyFromInputToEntity(CozinhaInput cozinhaInput, Cozinha cozinha) {
        cozinha.setNome(cozinhaInput.getNome());
    }
}
