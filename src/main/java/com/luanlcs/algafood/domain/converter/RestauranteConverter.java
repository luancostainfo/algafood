package com.luanlcs.algafood.domain.converter;

import com.luanlcs.algafood.api.dto.output.ListagemResturanteOutput;
import com.luanlcs.algafood.domain.model.Restaurante;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestauranteConverter {

    public ListagemResturanteOutput toOutput(Restaurante restaurante) {
        ListagemResturanteOutput listagemResturanteOutput = new ListagemResturanteOutput();
        listagemResturanteOutput.setId(restaurante.getId());
        listagemResturanteOutput.setNome(restaurante.getNome());
        listagemResturanteOutput.setTaxaFrete(restaurante.getTaxaFrete());
        listagemResturanteOutput.setCozinha(restaurante.getCozinha().getNome());
        return listagemResturanteOutput;
    }

    public List<ListagemResturanteOutput> toOutputList(List<Restaurante> restaurantes) {
        return restaurantes.stream().map(this::toOutput).collect(Collectors.toList());
    }
}
