package com.luanlcs.algafood.domain.repository;


import com.luanlcs.algafood.api.dto.filter.CozinhaFilter;
import com.luanlcs.algafood.domain.model.Cozinha;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class CozinhaRepositorySpecs {

    public static Specification<Cozinha> comFiltro(CozinhaFilter filter) {
        return (root, query, builder) -> {
            List<Predicate> predicados = new ArrayList<>();

            if (StringUtils.hasLength(filter.getNome())) {
                Predicate predicado = builder.like(root.get("nome"),  filter.getNome() + "%");
                predicados.add(predicado);
            }

            return builder.and(predicados.toArray(new Predicate[0]));
        };
    }
}
