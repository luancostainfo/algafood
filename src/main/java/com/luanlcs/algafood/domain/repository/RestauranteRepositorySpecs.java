package com.luanlcs.algafood.domain.repository;

import com.luanlcs.algafood.api.dto.filter.RestauranteFilter;
import com.luanlcs.algafood.domain.model.Cozinha;
import com.luanlcs.algafood.domain.model.Restaurante;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;

public class RestauranteRepositorySpecs {

    public static Specification<Restaurante> comFiltro(RestauranteFilter restauranteFilter) {
        return (root, query, builder) -> {
            ArrayList<Predicate> predicados = new ArrayList<>();

            root.fetch("cozinha");
            query.distinct(true);

            if (StringUtils.hasLength(restauranteFilter.getNome())) {
                Predicate predicado = builder.like(root.get("nome"), restauranteFilter.getNome() + "%");
                predicados.add(predicado);
            }

            if (restauranteFilter.getTaxaFreteInicial() != null) {
                Predicate predicado = builder.greaterThanOrEqualTo(root.get("taxaFrete"), restauranteFilter.getTaxaFreteInicial());
                predicados.add(predicado);
            }


            return builder.and(predicados.toArray(new Predicate[0]));
        };
    }

}
