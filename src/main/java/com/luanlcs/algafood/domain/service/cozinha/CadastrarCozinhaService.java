package com.luanlcs.algafood.domain.service.cozinha;

import com.luanlcs.algafood.api.dto.input.CozinhaInput;
import com.luanlcs.algafood.api.dto.output.CozinhaOutput;
import com.luanlcs.algafood.domain.converter.CozinhaConverter;
import com.luanlcs.algafood.domain.model.Cozinha;
import com.luanlcs.algafood.domain.repository.CozinhaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class CadastrarCozinhaService {

    private final CozinhaRepository cozinhaRepository;
    private final CozinhaConverter cozinhaConverter;


    @Transactional
    public CozinhaOutput executar(CozinhaInput cozinhaInput) {
        Cozinha cozinha = cozinhaConverter.toEntity(cozinhaInput);
        cozinha = cozinhaRepository.save(cozinha);
        return cozinhaConverter.toOutput(cozinha);
    }
}
