package com.luanlcs.algafood.domain.service.cozinha;

import com.luanlcs.algafood.api.dto.filter.CozinhaFilter;
import com.luanlcs.algafood.api.dto.output.CozinhaOutput;
import com.luanlcs.algafood.domain.converter.CozinhaConverter;
import com.luanlcs.algafood.domain.model.Cozinha;
import com.luanlcs.algafood.domain.repository.CozinhaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.luanlcs.algafood.domain.repository.CozinhaRepositorySpecs.*;
import static org.springframework.data.jpa.domain.Specification.where;

@RequiredArgsConstructor
@Service
public class ListarCozinhaService {

    private final CozinhaRepository cozinhaRepository;
    private final CozinhaConverter cozinhaConverter;

    public Page<CozinhaOutput> executar(CozinhaFilter cozinhaFilter, Pageable pageable) {
        Page<Cozinha> cozinhas = cozinhaRepository.findAll(comFiltro(cozinhaFilter), pageable);
        return cozinhas.map(cozinhaConverter::toOutput);
    }
}
