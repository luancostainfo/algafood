package com.luanlcs.algafood.domain.service.cozinha;

import com.luanlcs.algafood.domain.repository.CozinhaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class ExcluirCozinhaService {

    private final CozinhaRepository cozinhaRepository;

    @Transactional
    public void executar(Long id) {
        cozinhaRepository.deleteById(id);
        cozinhaRepository.flush();
    }
}
