package com.luanlcs.algafood.domain.service.cozinha;

import com.luanlcs.algafood.api.dto.output.CozinhaOutput;
import com.luanlcs.algafood.domain.converter.CozinhaConverter;
import com.luanlcs.algafood.domain.model.Cozinha;
import com.luanlcs.algafood.domain.repository.CozinhaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class BuscarCozinhaService {

    private final CozinhaRepository cozinhaRepository;
    private final CozinhaConverter cozinhaConverter;

    public CozinhaOutput executar(Long id) {
        Cozinha cozinha = cozinhaRepository.findById(id)
                .orElseThrow(() -> new EmptyResultDataAccessException("Cozinha não encontrada.", 1));

        return cozinhaConverter.toOutput(cozinha);
    }
}
