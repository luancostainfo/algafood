package com.luanlcs.algafood.domain.service.restaurante;

import com.luanlcs.algafood.api.dto.filter.RestauranteFilter;
import com.luanlcs.algafood.api.dto.output.ListagemResturanteOutput;
import com.luanlcs.algafood.domain.converter.RestauranteConverter;
import com.luanlcs.algafood.domain.model.Restaurante;
import com.luanlcs.algafood.domain.repository.RestauranteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.luanlcs.algafood.domain.repository.RestauranteRepositorySpecs.comFiltro;

@RequiredArgsConstructor
@Service
public class ListarRestauranteService {

    private final RestauranteRepository restauranteRepository;
    private final RestauranteConverter restauranteConverter;

    public Page<ListagemResturanteOutput> executar(RestauranteFilter restauranteFilter, Pageable pageable) {
        Page<Restaurante> restaurantes = restauranteRepository.findAll(comFiltro(restauranteFilter), pageable);
        return restaurantes.map(restauranteConverter::toOutput);
    }
}
