package com.luanlcs.algafood.domain.service.cozinha;

import com.luanlcs.algafood.api.dto.input.CozinhaInput;
import com.luanlcs.algafood.api.dto.output.CozinhaOutput;
import com.luanlcs.algafood.domain.converter.CozinhaConverter;
import com.luanlcs.algafood.domain.model.Cozinha;
import com.luanlcs.algafood.domain.repository.CozinhaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class AtualizarCozinhaService {

    private final CozinhaRepository cozinhaRepository;
    private final CozinhaConverter cozinhaConverter;

    @Transactional
    public CozinhaOutput executar(Long id, CozinhaInput cozinhaInput) {
        Cozinha cozinha = cozinhaRepository.findById(id)
                .orElseThrow(() -> new EmptyResultDataAccessException("Cozinha não encontrada.", 1));

        cozinhaConverter.copyFromInputToEntity(cozinhaInput, cozinha);

        return cozinhaConverter.toOutput(cozinha);
    }
}
