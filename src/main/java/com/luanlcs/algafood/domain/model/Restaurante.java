package com.luanlcs.algafood.domain.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Restaurante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String codigo;

    private String nome;

    private BigDecimal taxaFrete;

    @ManyToOne
    @JoinColumn(name = "cozinha_id")
    private Cozinha cozinha;

    @CreationTimestamp
    private OffsetDateTime dataHoraInclusao;

    @UpdateTimestamp
    private OffsetDateTime dataHoraAtualizacao;

    @PrePersist
    public void prePersist() {
        this.codigo = UUID.randomUUID().toString();
    }

}
